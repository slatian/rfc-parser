local json = require("cjson")

-- Copyright 2023 Slatian
-- You may use it under the therms and condition of
-- the AGPL-3.0-or-later

local mode,path = ...

function read_lines(path)
	local f = io.open(path)
	if not f then return nil end
	local lines = {}
	while true do
		line = f:read()
		if not line then break end
		lines[#lines+1] = line
	end
	f:close()
	return lines
end

function print_block(blocktype, text, prefix, level)
	local color = "00"
	local text_color = "00"
	if blocktype == "unknown" then
		color = "31;01"
		text_color = "01;31"
	elseif blocktype == "blank" then
		color = "30;01"
		text_color = "30"
	elseif blocktype == "pagebreak" then
		color = "34"
		text_color = "30"
	elseif blocktype == "heading" then
		color = "33;01"
		text_color = "33"
	elseif blocktype == "paragraph" then
		color = "32"
		text_color = "32"
	elseif blocktype == "note" then
		color = "32;01"
		text_color = "32;03"
	elseif blocktype == "definition" then
		color = "37"
		text_color = "37"
	end
	local heading_space = 10
	io.write("\x1b["..color.."m"..(" "):rep(heading_space-#blocktype)..blocktype.."\x1b[00;"..text_color.."m"..("  "):rep((level or 1)-1).."┃ ")
	if prefix then
		io.write("\x1b[01m"..prefix.."\x1b[00;"..text_color.."m ")
	end
	local text = text:gsub("\n", "\n"..(" "):rep(heading_space)..("  "):rep((level or 1)-1).."┃ ")
	print(text.."\x1b[00m")
end

function print_element_as_output(element, state)
	print_block(element.type, element.text or "-", element.section_number or element.bullet or element.key, element.level)
end

function test_blank_line(line)
	return line:match("^%s*$")
end

function get_indentation_level(line)
	return #line:match("^(%s*)")
end

function parse_content(lines, renderer)
	local state = {}
	local section = "start"
	local section_hint = ""
	local section_bullet = ""
	local section_level = 1
	local paragraph_mode_indentation = false
	local paragraph_mode_continue_accross_blanks = false
	local last_headline = ""
	local section_number_pattern = "[0-9][0-9.]*"
	local paragraph_abort_pattern = "\x00"
	local render = function (data) renderer(data,state) end
	-- resettable counters and accumulators
	local key = ""
	local acc = ""
	local blank_counter = 0
	local pagebreak_counter = 0
	-- paragraph mode trickery
	local paragraph_mode_mode = ""
	local held_back_pmode_blanks_before_page_break = 0
	local page_break_was_held_back = false
	local held_back_pmode_blanks_after_page_break = 0
	local reset_section_to_after_page_break = ""
	
	for n,line in ipairs(lines) do
		local is_blank_line = test_blank_line(line)
		if is_blank_line then
			blank_counter = blank_counter+1
		end
		local current_indentation = get_indentation_level(line)
		local run_main_section = true

		-- find pagebreak start
		if line:match("%[Page [0-9]+%]") then
			reset_section_to_after_page_break = section
			section = "pagebreak"
			if paragraph_mode_indentation then
				page_break_was_held_back = true
				paragraph_mode_mode = "after_page_break"
			else
				render{type="pagebreak"}
			end
			blank_counter = 0
			pagebreak_counter = pagebreak_counter + 1
			run_main_section = false
		-- find pagebreak end
		elseif section == "pagebreak" then
			if blank_counter == 2 then
				section = reset_section_to_after_page_break
			end
			run_main_section = false
		-- paragraph mode
		elseif paragraph_mode_indentation then
			--print("pmode", paragraph_mode_indentation, "current: "..current_indentation..(is_blank_line and " blank" or " xyz"), paragraph_mode_mode)
			if
				(
					current_indentation == paragraph_mode_indentation or
					(
						(tonumber(paragraph_mode_indentation) or 0) < 0 and
						current_indentation >= -(tonumber(paragraph_mode_indentation) or 0)
					)or
					(paragraph_mode_indentation == true and current_indentation > 0)
				) and (
					paragraph_mode_mode ~= "lookahead" or
					paragraph_mode_continue_accross_blanks
				) and (
					not line:match(paragraph_abort_pattern)
				)
			then
				-- inset a newline if we did some skipping
				if paragraph_mode_continue_accross_blanks and held_back_pmode_blanks_before_page_break > 0 then
					acc = acc.."\n"
				end
				if section == "code" then
					acc = acc.."\n"..line:gsub(
						"^"..(" "):rep(
							-math.min(tonumber(paragraph_mode_indentation) or 0, 0)
						),"")
				else
					assert(acc, "Acc is nil, this shouldn't happen, section: "..tostring(section))
					acc = acc.."\n"..line:gsub("^%s+", "")
					-- lock indentation
					if paragraph_mode_indentation == true then
						paragraph_mode_indentation = current_indentation
					end
				end
				run_main_section = false
				held_back_pmode_blanks_before_page_break = 0
				held_back_pmode_blanks_after_page_break = 0
				paragraph_mode_mode = ""
			-- continue looking ahead if there is a blank line,
			-- but not at the end of a sentence
			elseif is_blank_line and (not acc:match("[.:]$") or paragraph_mode_continue_accross_blanks) then
				-- disable the scanning across blank lines if we were unable to lock the indentation level
				if paragraph_mode_indentation == true then
					paragraph_mode_continue_accross_blanks = false
				end
				if paragraph_mode_mode == "" or paragraph_mode_mode == "lookahead" then
					paragraph_mode_mode = "lookahead"
					held_back_pmode_blanks_before_page_break = held_back_pmode_blanks_before_page_break + 1
				elseif paragraph_mode_mode == "after_page_break" then
					held_back_pmode_blanks_after_page_break = held_back_pmode_blanks_after_page_break + 1
				end
			elseif section ~= "text" then
				local element = {
					type = section,
					text = acc,
				}
				if key ~= "" then
					element.key = key
				end
				if section_hint ~= "" then
					element.hint = section_hint
				end
				if section_level > 1 then
					element.level = section_level
				end
				if section_bullet ~= "" then
					element.bullet = section_bullet
				end
				render(element)
				acc = ""
				key = ""
				section = "text"
				section_hint = ""
				section_level = 1
				section_bullet = ""
				paragraph_mode_indentation = false
				paragraph_mode_continue_accross_blanks = false
				paragraph_mode_mode = ""
				paragraph_abort_pattern = "\x00"
				for i = 1,held_back_pmode_blanks_before_page_break do
					render{type="blank"}
				end
				if page_break_was_held_back then
					render{type="pagebreak"}
				end
				for i = 1,held_back_pmode_blanks_after_page_break do
					render{type="blank"}
				end
				page_break_was_held_back = false
				held_back_pmode_blanks_before_page_break = 0
				held_back_pmode_blanks_after_page_break = 0
			end
		end

		if not run_main_section then
			-- noop
		-- skip blank lines at the start
		elseif section == "start" then
			if not is_blank_line then
				section = "metadata"
				blank_counter = 0
			end
		-- skip metadata for now
		elseif section == "metadata" then
			if is_blank_line then
				section = "title"
			end
		-- parse document title
		elseif section == "title" then
			if not is_blank_line then
				render{type="title", text=line:match("^%s*(.-)%s*$")}
				section="subtitle"
			end
		-- parse document subtitle
		elseif section == "subtitle" then
			if not is_blank_line then
				render{type="subtitle", text=line:match("^%s*%((.-)%)%s*$")}
			end
			section = "text"
		-- match table of contents entries
		elseif line:match("^ ") and last_headline == "Table of Contents"  then
			if line:match("^  +[0-9.]+ +") then
				section = "toc-item"
				key = line:match("^ +([0-9.]+)")
				acc = line:gsub("^ +[0-9.]+ +","")
				paragraph_mode_indentation = #(line:match("^ +[0-9.]+ +") or line)
				paragraph_abort_pattern = "^ +[0-9]"
				-- … someone had fun with formatting in RFC6756
			elseif line:match("^  .+[0-9]+$") then
				render{
					type="toc-item",
					text=line:match("^ +(.+)")
				}
			end
		-- find bnf definitions
		elseif line:match("::=") then
			section = "code"
			section_hint = "bnf"
			acc = line
			paragraph_mode_indentation = true
		-- parse notes
		elseif line:match("^ *Note: ") then
			section = "note"
			acc = line:match("^ *.- +(.*)")
			paragraph_mode_indentation = true
		-- find references used in newer RFCs is the References sections
		elseif line:match("^   %[.-%]") and last_headline:match("References") then
			section = "reference"
			key = line:match("^   %[(.-)%]")
			acc = line:match("^   %[.-%] +(.*)") or ""
			paragraph_mode_indentation = true
			paragraph_mode_continue_accross_blanks = true
		-- find definitions used in newer RFCs
		elseif line:match("^   [^ ].-:  ") and
			(get_indentation_level(lines[n+1]) == 6 or test_blank_line(lines[n+1]))
		then
			section = "definition"
			key = line:match("^   ([^ ].-):  ")
			acc = line:match("^   [^ ].-:  (.*)")
			paragraph_mode_indentation = 6
		-- find level 2 definitions used in newer RFCs
		elseif line:match("^      [^ ].-:  ") then
			section = "definition"
			section_level = 2
			key = line:match("^      ([^ ].-):  ")
			acc = line:match("^      [^ ].-:  (.*)")
			paragraph_mode_indentation = 9
		-- try to identify headings
		elseif (
			line:match("^[A-Z]%w") or
			line:match("^"..section_number_pattern.." +[A-Z]")
			) and test_blank_line(lines[n-1]) and test_blank_line(lines[n+1])
		then
			last_headline = line
			render{
				type="heading",
				text=line:gsub("^[0-9.]+%s+",""),
				section_number=line:match("^[0-9.]+")
			}
			-- reconfigure sections numbers to be more restrictive if there is always a dot after the first number, i.e. RFC1436 and RFC3465 clash otherwise
			if line:match("^[0-9]+%. ") then
				section_number_pattern="[0-9]+%.[0-9.]*"
			end
		-- find definitions used in older RFCs (i.e. RFC 1436)
		-- the second (not line:match) stops it from matching ascii art too easily
		elseif (current_indentation == 0 or current_indentation == 3) and line:match("[^.]   ") and (not line:match("[^.]   +[^ ].+   ") and (not line:match("          "))) then
			section = "definition"
			key = line:match("^%s*(.*[^.])   ")
			acc = line:match("[^.]   +(.*)")
			-- RFC2119 has some funny list definition hybrid that is formatted like a list but not indented and therefore a definition
			if not lines[n+1]:match("[^.]   ") then
				paragraph_mode_indentation = true
			else
				paragraph_mode_indentation = #line:match("^%s*.*[^.]   +")
			end
		-- parse lists
		elseif
			line:match("^    ? ? ?[0-9]+%. ") or
			line:match("^      %([^%s]+%) ") or
			line:match("^    ? ? ?[a-z0-9][0-9]?%) ") or
			line:match("^   o  ")
		then
			section = "list-item"
			section_bullet = line:match("^ +(.-) ")
			acc = line:gsub("^ +[^ ]+ +","")
			if section_bullet == "o" then
				paragraph_mode_indentation = #line:match("^ +[^ ]+ +")
			else
				paragraph_mode_indentation_on_blank = #line:match("^ +[^ ]+ +")
				paragraph_mode_indentation = true
			end
		-- parse paragraphs
		elseif line:match("^   [^ ]") then
			section = "paragraph"
			acc = line:gsub("^   ","")
			paragraph_mode_indentation = current_indentation
		-- parse level 2 paragraphs
		elseif line:match("^      [^ ]") then
			section = "paragraph"
			section_level = 2
			acc = line:gsub("^      ","")
			paragraph_mode_indentation = current_indentation
		-- parse code blocks
		elseif line:match("^    [^ ]") then
			section = "code"
			acc = line:gsub("^    ","")
			paragraph_mode_indentation = -4
			paragraph_mode_continue_accross_blanks = true
		-- pass on blank lines
		elseif is_blank_line then
			if not paragraph_mode_indentation then
				render{type="blank"}
			end
		-- pass on unknown lines for debugging
		else
			render{type="unknown", text=line}
		end
	end
	return state
end

if mode == "parse-content" then
	local lines = read_lines(path)
	if not lines then
		io.stderr:write("Unable to read file \n")
		os.exit(1)
	end
	parse_content(lines, print_element_as_output)
else
	print("unknown mode")
end
