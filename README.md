# RFC Parser

This is an experimental script that takes RFCs and parses them into a form that is easier to convert to semantic HTML than the original with the goal being a script that acts similar to a markdown parser but for RFCs.

Currently it is able to tag text blocks mostly correctly, hover it trips over ascii art sometimes and completely falls apart with the very first RFCs.

You can obtain [an archive of all RFCs from rfc-editor.org](https://www.rfc-editor.org/rfc/tar/).

Usage: `lua process-rfc.lua parse-content <path>`

## Still TODO

* parse metadata
* clean up around pagebreaks
* Use some heuristics to find out wheather or not a paragraph is supposed to be preformatted.
* Interpret indedet paragraphs as code, quotes, lists etc.
* Fix misclassified elements around unknown blocks
* Build a dicument tree
* Identify inline elements
* Render to HTML

## Licensing

This script is licensed under an

Licensing information about RFCs can be found at https://trustee.ietf.org/documents/trust-legal-provisions/

<b>Note:</b> They say that you are not allowed to publish modified RFCs which is the category the output of this script probably falls under. As I'm not a lawyer my advice is to ask the ietf/RFC Authors if publising of this scripts output would be okay.

